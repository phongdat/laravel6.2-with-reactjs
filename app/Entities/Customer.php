<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Support\DataTablePaginate;

class Customer extends Model
{
    use DataTablePaginate;
    protected $table = 'customers';
    protected $fillable = ['name', 'email', 'phone', 'address'];
    protected $hidden = [];
}
