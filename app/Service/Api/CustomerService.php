<?php
namespace App\Service\Api;
use App\Entities\Customer;

class CustomerService {

    public function index() {
        return response()
            ->json([
                'model' => Customer::DataTablePaginate()
            ]);
    }

    public function create() {

    }

    public function store() {

    }

    public function edit() {

    }

    public function update() {

    }

    public function destroy() {

    }
}
