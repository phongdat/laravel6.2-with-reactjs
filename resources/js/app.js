require('./bootstrap');
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import RootComponent from './components/RootComponent';

export default class Root extends Component{
    render() {
        return (
            <RootComponent></RootComponent>
        )

    }
}

if(document.getElementById("root")) {
    ReactDOM.render(<Root />, document.getElementById("root"));
}
