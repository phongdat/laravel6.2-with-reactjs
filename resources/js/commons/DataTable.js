import React, {Component} from 'react';
import {apiRequest} from "../helpers/apiRequest";
import {buildUrl} from "../helpers/buildUrl";
import {pagination} from "../helpers/pagination";
import { Slot } from 'react-slot';

class DataTable extends Component {

    constructor(props) {
        super(props);
        this.state = ({
            model: {
                data: []
            },
            numberWithDots: [],
            params: props.params,
            theads: props.theads
        });
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        apiRequest('/api/customer'+ buildUrl(this.state.params))
            .then(res=> {
                let m = res.model;
                this.setState({
                    ...this.state,
                    model: m,
                    numberWithDots: pagination(m.current_page, m.last_page)
                })
            })
            .catch(err => console.log(err))
    }

    renderTheads() {
        const {theads} = this.state;
        return theads.map((val, index) => {
            if(val.sort) {
                if(this.state.params.sort_column === val.key) {
                    return (
                        <th key={index} scope="col" onClick={() => this.sort(val.key)} style={{width: val.width}}>
                            {val.title}
                            <i className={this.state.params.direction === 'desc' ?
                                'fas fa-sort-down align-text-top' :
                                'fas fa-sort-up align-bottom'}
                            />
                        </th>
                    )
                } else {
                    return (
                        <th key={index} scope="col" onClick={() => this.sort(val.key)}>
                            {val.title}
                        </th>
                    )
                }
            } else {
                return (
                    <th key={index} scope="col">{val.title}</th>
                )
            }
        });
    }

    renderTbody() {
        let data = this.state.model.data;
        if(data.length > 0) {
            return data.map((item, index) => {
                return (
                    <tr key={index}>
                        <th scope="row">{item.id}</th>
                        <td>{item.name}</td>
                        <td>{item.email}</td>
                        <td>{item.phone}</td>
                        <td>{item.address}</td>
                    </tr>
                )
            })
        }
    }

    renderPaginate() {
        let pages = this.state.numberWithDots;
        if(pages.length > 0) {
            return pages.map((val, index) => {
                let liClass = 'page-item';
                if(this.state.params.page === val) {
                    liClass = 'page-item active';
                }
                return (
                    <li key={index} className={liClass}>
                        <a className="page-link" href="#" onClick={() => this.changePerPage(val)}>{val}</a>
                    </li>
                )
            })
        }
    }

    changePerPage(val) {
        event.preventDefault();
        this.setState({
            ...this.state,
            params: {
                ...this.state.params,
                page: val
            }
        }, () => this.fetchData());
    }

    prev() {
        event.preventDefault();
        this.setState({
            ...this.state,
            params: {
                ...this.state.params,
                page: this.state.params.page - 1
            }
        }, () => this.fetchData());
    }

    next() {
        event.preventDefault();
        this.setState({
            ...this.state,
            params: {
                ...this.state.params,
                page: this.state.params.page + 1
            }
        }, () => this.fetchData());
    }

    sort(key) {
        this.setState({
            ...this.state,
            params: {
                ...this.state.params,
                sort_column: key,
                direction: (this.state.params.direction === "desc") ? 'asc' : 'desc'
            }
        }, () => this.fetchData());
    }

    // sendData(){
    //     let data = this.state.model.data;
    //     this.props.parentCallback(data);
    // }


    render() {
        //const { children } = this.props;
        const  model = this.state.model;
        return (
            <div className="card">
                <div className="card-header">
                    List Customer
                </div>
                <div className="card-body">
                    <table className="table table-hover">
                        <thead>
                        <tr>
                            {this.renderTheads()}
                        </tr>
                        </thead>
                        <tbody>
                            {this.renderTbody()}
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">
                        <ul className="pagination">
                            <li className={model.prev_page_url === null ? 'page-item disabled' : 'page-item'}>
                                <a className="page-link"
                                   href="#"
                                   aria-label="Previous"
                                   onClick={() => this.prev()}>
                                    <span aria-hidden="true">«</span>
                                    <span className="sr-only">Previous</span>
                                </a>
                            </li>
                            {this.renderPaginate()}
                            <li className={model.next_page_url === null ? 'page-item disabled' : 'page-item'}>
                                <a className="page-link"
                                   href="#"
                                   aria-label="Next"
                                   onClick={() => this.next()}>
                                    <span aria-hidden="true">»</span>
                                    <span className="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        )
    }
}

export default DataTable;
