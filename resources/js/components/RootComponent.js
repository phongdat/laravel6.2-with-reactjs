import React, {Component} from 'react';
import ListCustomer from './customer/ListComponent';
import {
    Router,
    Switch,
    Route,
    NavLink} from 'react-router-dom'
import Nav from './layouts/Nav';
import routes from './../routes';
import history from './../history';

const showRoutes = () => {
    if(routes.length > 0) {
        return routes.map((item, index) => {
            return <Route key={index} path={item.path} exact={item.exact}>{item.main}</Route>;
        })
    }
};


class RootComponent extends Component{
    render() {
        return (
            <div className="container">
                <Router history={history}>
                    <Nav></Nav>
                    <div className="container">
                        <Switch>
                            {showRoutes()}
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    }
}

export default RootComponent;


