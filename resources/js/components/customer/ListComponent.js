import React, {Component} from 'react';
import './Customer.css';
import DataTable from './../../commons/DataTable';

class ListComponent extends React.Component{
    constructor(props) {
        super(props);
        this.state = ({
            params: {
                page: 1,
                sort_column: 'id',
                direction: 'desc',
                per_page: 10
            },
            theads: [
                {title: '#ID', key: 'id', sort: true, width: '75px'},
                {title: 'Name', key: 'name', sort: true},
                {title: 'Email', key: 'email', sort: true},
                {title: 'Phone', key: 'phone', sort: false},
                {title: 'Address', key: 'address', sort: false}
            ]
        });
    }
    //
    // renderTbody(data) {
    //     let data = this.state.model.data;
    //     if(data.length > 0) {
    //         return data.map((item, index) => {
    //             return (
    //                 <tr key={index}>
    //                     <th scope="row">{item.id}</th>
    //                     <td>{item.name}</td>
    //                     <td>{item.email}</td>
    //                     <td>{item.phone}</td>
    //                     <td>{item.address}</td>
    //                 </tr>
    //             )
    //         })
    //     }
    // }


    render() {
        const {params, theads} = this.state;
        return (
            <DataTable params={params} theads={theads}>
            </DataTable>
        )
    }
}

export default ListComponent;
