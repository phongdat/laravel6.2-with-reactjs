import React from 'react';
import ListCustomer from './components/customer/ListComponent';
import ListInvoice from './components/invoice/ListComponent';

const routes = [
    {
        path: '/customer',
        exact: true,
        main: () => <ListCustomer />
    },
    {
        path: '/invoice',
        exact: true,
        main: () => <ListInvoice />
    }
]

export default routes;
